import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import { SignUpLink } from '../SignUp';
import { PasswordForgetLink } from '../PasswordForget';
import { withFirebase } from '../Firebase';
import * as ROUTES from '../../constants/routes';

const SignInPage = () => (
  <div className="main-content">
    {/* <h1>SignIn</h1> */}
    <SignInForm />
    {/* <PasswordForgetLink /> */}
    {/* <SignUpLink /> */}
  </div>
);

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null
};

class SignInFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = (event) => {
    const { email, password } = this.state;

    this.props.firebase
      .doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState({ ...INITIAL_STATE });
        this.props.history.push(ROUTES.HOME);
      })
      .catch((error) => {
        this.setState({ error });
      });

    event.preventDefault();
  };

  onChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { email, password, error } = this.state;

    const isInvalid = password === '' || email === '';

    return (
      <div className="auth-form">
        <form onSubmit={this.onSubmit}>
          <div className="auth-form-header">
            <h1>Sign In</h1>
          </div>
          <div className="auth-form-body">
            <label htmlFor="email_field">Email address</label>
            <input
              name="email"
              value={email}
              onChange={this.onChange}
              type="text"
              className="input-block"
              id="email_field"
              autoFocus="autofocus"
              tab-index="1"
            />
            <label htmlFor="password">
              Password <PasswordForgetLink />
            </label>
            <input
              name="password"
              value={password}
              onChange={this.onChange}
              type="password"
              className="input-block"
              id="password"
              tab-index="2"
            />
            <button disabled={isInvalid} type="submit" className="signin-btn">
              Sign In
            </button>

            {error && <p>{error.message}</p>}
          </div>
        </form>
        <p className="create-account-callout">
          <SignUpLink />
        </p>
      </div>
    );
  }
}

const SignInForm = compose(
  withRouter,
  withFirebase
)(SignInFormBase);

export default SignInPage;

export { SignInForm };
